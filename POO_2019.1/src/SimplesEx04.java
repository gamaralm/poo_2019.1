import java.util.Scanner;

public class SimplesEx04 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 03
		 * 
		 * Escreva um programa para calcular e imprimir o n�mero de l�mpadas necess�rias para iluminar um
		 * determinado c�modo de uma resid�ncia. Dados de entrada: a pot�ncia da l�mpada utilizada (em
		 * watts), as dimens�es (largura e comprimento, em metros) do c�modo. Considere que a pot�ncia
		 * necess�ria � de 18 watts por metro quadrado.
		 */
		
		Scanner scanner = new Scanner(System.in);
		double potencia, largura, comprimento, lampadas;
		
		System.out.print("Pot�ncia (watts): ");
		potencia = scanner.nextDouble();
		System.out.print("Largura (metros): ");
		largura = scanner.nextDouble();
		System.out.print("Comprimento (metros): ");
		comprimento = scanner.nextDouble();
		
		lampadas = (largura * comprimento * 18) / potencia;
		
		System.out.println("L�mpadas = " + lampadas);
		
	}

}
