import java.text.DecimalFormat;
import java.util.Scanner;

public class SimplesEx06 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 06
		 * 
		 * Um motorista de t�xi deseja calcular o rendimento de seu carro na pra�a. Sabendo-se que o pre�o
		 * do combust�vel � de R$ 1,90, escreva um programa para ler: a marca��o do od�metro (Km) no in�cio
		 * do dia, a marca��o (Km) no final do dia, o n�mero de litros de combust�vel gasto e o valor total (R$)
		 * recebido dos passageiros. Calcular e escrever: a m�dia do consumo em Km/L e o lucro (l�quido) do dia.
		 */
		
		Scanner scanner = new Scanner(System.in);
		DecimalFormat formato = new DecimalFormat("#,##0.00");
		final double CUSTO_COMBUSTIVEL = 1.90;
		double odometroInicio, odometroFim, consumoTotal, receitaBruta, consumoMedia, receitaLiquida;
		
		System.out.print("In�cio Od�metro (Km): ");
		odometroInicio = scanner.nextDouble();
		System.out.print("Fim Od�metro (Km): ");
		odometroFim = scanner.nextDouble();
		System.out.print("Combust�vel Gasto (Litros): ");
		consumoTotal = scanner.nextDouble();
		System.out.print("Valor Recebido (R$): ");
		receitaBruta = scanner.nextDouble();
		
		consumoMedia = (odometroFim-odometroInicio)/consumoTotal;
		receitaLiquida = receitaBruta-(consumoTotal*CUSTO_COMBUSTIVEL);
		
		System.out.println("M�dia do Consumo (Km/L): " + consumoMedia);
		System.out.print("Lucro: R$" + formato.format(receitaLiquida));
	}

}
