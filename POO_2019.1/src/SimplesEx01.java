import java.util.Scanner;

public class SimplesEx01 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 01
		 * 
		 * Escreva um programa para ler o raio de um c�rculo, calcular e escrever a sua �rea.
		 */
		
		Scanner scanner = new Scanner(System.in);
		double raio;
		
		System.out.print("Raio: ");
		raio = scanner.nextDouble();
				
		System.out.println("�rea = " + (Math.PI * raio * raio));
		
	}

}