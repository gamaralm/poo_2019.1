import java.util.Scanner;

public class SimplesEx02 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 02
		 * 
		 * Escreva um programa para ler uma temperatura em graus Fahrenheit, calcular e escrever o valor 
		 * correspondente em graus Celsius.
		 */
		
		Scanner scanner = new Scanner(System.in);
		double fahrenheit, celsius;
		
		System.out.print("Temperatura (�F): ");
		fahrenheit = scanner.nextDouble();
		
		celsius =  (fahrenheit-32)*5/9;
		
		System.out.println("Temperatura (�C) = " + celsius);
		
	}

}