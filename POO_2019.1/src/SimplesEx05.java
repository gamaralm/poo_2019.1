import java.util.Scanner;

public class SimplesEx05 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 05
		 * 
		 * Escreva um programa para ler as dimens�es de uma cozinha retangular (comprimento, largura e
		 * altura), calcular e escrever a quantidade de caixas de azulejos para se colocar em todas as suas
		 * paredes (considere que n�o ser� descontada a �rea ocupada por portas e janelas). Cada caixa de
		 * azulejos possui 1,5 m2.
		 */
		
		Scanner scanner = new Scanner(System.in);
		double largura, comprimento, altura, azulejos, areaTotal;
		
		System.out.print("Altura (metros): ");
		altura = scanner.nextDouble();
		System.out.print("Largura (metros): ");
		largura = scanner.nextDouble();
		System.out.print("Comprimento (metros): ");
		comprimento = scanner.nextDouble();
		
		areaTotal = ((altura*largura)+(altura*comprimento))*2;
		azulejos = areaTotal/1.5;
		
		System.out.println("Caixas de Azulejos = " + azulejos);
		
	}

}
