import java.util.Scanner;

public class SimplesEx07 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 07
		 * 
		 * A equipe Benneton-Ford deseja calcular o n�mero m�nimo de litros que dever� colocar no tanque
		 * de seu carro para que ele possa percorrer um determinado n�mero de voltas at� o primeiro
		 * reabastecimento. Escreva um programa que leia o comprimento da pista (em metros), o n�mero total
		 * de voltas a serem percorridas no grande pr�mio, o n�mero de reabastecimentos desejados e o
		 * consumo de combust�vel do carro (em Km/L). Calcular e escrever o n�mero m�nimo de litros
		 * necess�rios para percorrer at� o primeiro reabastecimento. OBS: Considere que o n�mero de voltas
		 * entre os reabastecimentos � o mesmo.
		 */
		
		Scanner scanner = new Scanner(System.in);
		double comprimento, totalVoltas, qtdReabastecimentos, consumo, combustivel;
		
		System.out.print("Comprimento (Metros): ");
		comprimento = scanner.nextDouble();
		System.out.print("Total de Voltas: ");
		totalVoltas = scanner.nextInt();
		System.out.print("Reabastecimentos: ");
		qtdReabastecimentos = scanner.nextInt();
		System.out.print("Consumo (Km/L): ");
		consumo = scanner.nextDouble();
		
		combustivel = (comprimento/1000)/totalVoltas/consumo;
		
		System.out.print("M�nimo de Combust�vel (Litros): " + combustivel);
	}

}
