import java.util.Scanner;

public class SimplesEx03 {

	public static void main(String[] args) {

		/**
		 * Exerc�cio 03
		 * 
		 * Escreva um programa para ler uma temperatura em graus Celsius, calcular e escrever o valor
		 * correspondente em graus Fahrenheit.
		 */
		
		Scanner scanner = new Scanner(System.in);
		double celsius, fahrenheit;
		
		System.out.print("Temperatura (�C): ");
		celsius = scanner.nextDouble();
		
		fahrenheit = (celsius*9/5)+32;
		
		System.out.println("Temperatura (�F) = " + fahrenheit);
		
	}

}